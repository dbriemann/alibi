package alibi

var (
	defaultVertexShaderSrc   string
	defaultVertexShader      *Shader
	defaultFragmentShaderSrc string
	defaultFragmentShader    *Shader
	defaultVertexFormat      AttributeFormat
	defaultShaderProgram     *Program
	defaultRenderer          *Renderer
)

// initDefaults must be called after a gl context has been initialized
// and after all init functions have run. Thus it cannot be an init
// function.
func initDefaults() {
	platformInitDefaults()

	defaultVertexFormat = AttributeFormat{
		Attribute{"aPos", Vec2},
		Attribute{"aColor", Vec4},
		Attribute{"aTexCoord", Vec2},
	}

	prog, err := NewProgram(nil, nil)
	if err != nil {
		panic(err.Error())
	}

	defaultShaderProgram = prog

	defaultRenderer = &Renderer{
		Program:      defaultShaderProgram,
		VertexFormat: &defaultVertexFormat,
	}
}
