//+build linux windows darwin freebsd
//+build !android !ios
//+build !js

package alibi

func platformInitDefaults() {
	defaultVertexShaderSrc = `
#version 330 core

in vec2 aPos;
in vec4 aColor;
in vec2 aTexCoord;

out vec4 vColor;
out vec2 vTexCoord;

uniform mat3 uTransform;

void main()
{	
	vec2 tpos = (uTransform * vec3(aPos, 1.0)).xy;
	gl_Position = vec4(tpos, 0.0, 1.0);
	vColor = aColor;
	vTexCoord = aTexCoord;
}
	`
	defaultVertexShader = NewShader(defaultVertexShaderSrc, VERTEX_SHADER)

	defaultFragmentShaderSrc = `
#version 330 core

in vec4 vColor;
in vec2 vTexCoord;

out vec4 fragColor;

uniform sampler2D uTexture;

void main()
{
	fragColor = texture(uTexture, vTexCoord);
}
	`
	defaultFragmentShader = NewShader(defaultFragmentShaderSrc, FRAGMENT_SHADER)
}
