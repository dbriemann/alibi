//+build js
//+build !linux !windows !darwin !freebsd
//+build !android !ios

package alibi

func platformInitDefaults() {
	defaultVertexShaderSrc = `#version 100

	attribute vec2 aPos;
	attribute vec4 aColor;
	attribute vec2 aTexCoord;
	
	varying vec4 vColor;
	varying vec2 vTexCoord;
	
	void main()
	{
		gl_Position = vec4(aPos, 0.0, 1.0);
		vColor = aColor;
		vTexCoord = aTexCoord;
	}
	`
	defaultVertexShader = NewShader(defaultVertexShaderSrc, VERTEX_SHADER)

	defaultFragmentShaderSrc = `#version 100
	precision mediump float;

	varying vec4 vColor;
	varying vec2 vTexCoord;

	uniform sampler2D uTexture;

	void main()
	{
		gl_FragColor = texture2D(uTexture, vTexCoord);
	}
	`
	defaultFragmentShader = NewShader(defaultFragmentShaderSrc, FRAGMENT_SHADER)
}
