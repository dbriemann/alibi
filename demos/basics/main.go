package main

import (
	"github.com/dbriemann/alibi"
)

type Game struct {
}

func (g *Game) Update(dt float64) {
}

func (g *Game) Draw(c *int) {
}

func main() {
	game := Game{}
	config := alibi.Config{
		Title:      "Basics Example",
		Width:      800,
		Height:     600,
		Fullscreen: false,
		VSync:      false,
	}
	engine := alibi.NewEngine(&game, config)
	engine.Run() // Must be called in main() function.
}
