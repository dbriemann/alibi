package alibi

import (
	"fmt"
	"math"
	"time"
)

type Game interface {
	Update(dt float64)
	Draw(c *int)
}

type Engine struct {
	Game Game

	config    Config
	window    Window
	frameTime float64
	fps       uint
}

func NewEngine(g Game, c Config) Engine {
	e := Engine{
		Game:   g,
		config: c,
		window: NewWindow(),
	}

	e.SetTickDuration(10 * time.Millisecond)
	return e
}

func (e *Engine) FPS() uint {
	return e.fps
}

func (e *Engine) SetTickDuration(length time.Duration) {
	e.frameTime = length.Seconds()
}

func (e *Engine) Run() {
	// Init window
	e.window.Init(&e.config)
	defer e.window.Destroy()

	initDefaults()

	var lag float64

	start := time.Now()
	last := start

	up := start
	var frames uint

	for !e.window.Closed() {
		current := time.Now()
		elapsed := current.Sub(last)
		last = current
		lag += elapsed.Seconds()

		dirty := false
		// Call update and draw functions.
		for lag >= e.frameTime {
			e.Game.Update(e.frameTime)
			lag -= e.frameTime
			dirty = true
		}

		if dirty {
			e.Game.Draw(nil)
			frames++
		} else {
			// Skipping the draw step because there was no update.
		}

		snap := current.Sub(up).Seconds()

		if snap > 1.0 {
			e.fps = uint(math.Round(float64(frames) / snap))
			fmt.Println("FPS", e.fps) // TODO remove
			up = current
			frames = 0
		}

		// Poll events and swap buffers, etc.
		e.window.Update()

		sleepFor := (e.frameTime - current.Sub(last).Seconds()) * 0.99
		time.Sleep(time.Duration(sleepFor * float64(time.Second)))
	}

	fmt.Println("bye")
}
