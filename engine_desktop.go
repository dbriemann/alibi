//+build linux windows darwin freebsd

package alibi

import "runtime"

func init() {
	runtime.LockOSThread()
}
