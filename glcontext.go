package alibi

import "github.com/go-gl/mathgl/mgl32"

var (
	context *glContext
)

// TODO binds should save last bound object.

type glContext struct {
}

func (c *glContext) SetClearColor(r, g, b, a float32) {
	setClearColor(r, g, b, a)
}

func (c *glContext) Clear(flags GlUint32) {
	clear(flags)
}

func (c *glContext) Version() string {
	return version()
}

func (c *glContext) SetViewport(x, y, width, height GlInt32) {
	setViewport(x, y, width, height)
}

func (c *glContext) DeleteShader(s GlObj) {
	deleteShader(s)
}

func (c *glContext) CreateProgram() GlObj {
	return createProgram()
}

func (c *glContext) AttachShader(prog, s GlObj) {
	attachShader(prog, s)
}

func (c *glContext) DetachShader(prog, s GlObj) {
	detachShader(prog, s)
}

func (c *glContext) LinkProgram(p GlObj) error {
	linkProgram(p)
	return testProgramLinkError(p)
}

func (c *glContext) CreateShader(stype GlUint32) GlObj {
	return createShader(stype)
}

func (c *glContext) SetShaderSource(s GlObj, src string) {
	setShaderSource(s, src)
}

func (c *glContext) CompileShader(s GlObj) error {
	compileShader(s)
	return testShaderCompileError(s)
}

func (c *glContext) UseProgram(p GlObj) {
	useProgram(p)
}

func (c *glContext) DeleteProgram(p GlObj) {
	deleteProgram(p)
}

func (c *glContext) CreateTexture() GlObj {
	return genTexture()
}

func (c *glContext) DeleteTexture(t GlObj) {
	deleteTexture(t)
}

func (c *glContext) BindTexture(tt GlUint32, t GlObj) {
	bindTexture(tt, t)
}

func (c *glContext) SetTextureParamInt(tt, pname GlUint32, pval GlInt32) {
	texParameteri(tt, pname, pval)
}

func (c *glContext) TexImage2D(width, height GlInt32, pix []uint8) {
	texImage2D(width, height, pix)
}

func (c *glContext) CreateBuffer() GlObj {
	return genBuffer()
}

func (c *glContext) DeleteBuffer(b GlObj) {
	deleteBuffer(b)
}

func (c *glContext) BindBuffer(btype GlUint32, buf GlObj) {
	bindBuffer(btype, buf)
}

func (c *glContext) SetBufferData(btype GlUint32, va []float32, dtype GlUint32) {
	bufferData(btype, va, dtype)
}

func (c *glContext) CreateVertexArray() GlObj {
	return genVertexArray()
}

func (c *glContext) DeleteVertexArray(va GlObj) {
	deleteVertexArray(va)
}

func (c *glContext) BindVertexArray(va GlObj) {
	bindVertexArray(va)
}

func (c *glContext) VertexAttribPointer(i GlUint32, size GlInt32, atype GlUint32, norm bool, stride GlInt32, offset GlInt) {
	vertexAttribPointer(i, size, atype, norm, stride, offset)
}

func (c *glContext) EnableVertexAttribArray(i GlUint32) {
	enableVertexAttribArray(i)
}

func (c *glContext) GetUniformLocation(p GlObj, name string) GlLoc {
	return getUniformLocation(p, name)
}

func (c *glContext) Uniform1i(loc GlLoc, val GlInt32) {
	uniform1i(loc, val)
}

func (c *glContext) UniformMatrix3fv(loc GlLoc, val mgl32.Mat3) {
	uniformMatrix3fv(loc, val)
}
