//+build linux windows darwin freebsd
//+build !android !ios
//+build !js

package alibi

import (
	"fmt"
	"unsafe"

	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/go-gl/mathgl/mgl32"
)

func newOpenGLContext() {
	// TODO return error instead of panic?
	if err := gl.Init(); err != nil {
		panic(err)
	}
	context = &glContext{}

	fmt.Println("OpenGL version", context.Version())

	context.SetClearColor(0.8, 0.3, 0.01, 1)
	context.Clear(COLOR_BUFFER_BIT)
}

func setClearColor(r, g, b, a float32) {
	gl.ClearColor(r, g, b, a)
}

func clear(flags GlUint32) {
	gl.Clear(flags)
}

func version() string {
	return gl.GoStr(gl.GetString(VERSION))
}

func setViewport(x, y, width, height GlInt32) {
	gl.Viewport(x, y, width, height)
}

func deleteShader(s GlObj) {
	gl.DeleteShader(s)
}

func createProgram() GlObj {
	return gl.CreateProgram()
}

func attachShader(prog, s GlObj) {
	gl.AttachShader(prog, s)
}

func detachShader(prog, s GlObj) {
	gl.DetachShader(prog, s)
}

func linkProgram(p GlObj) {
	gl.LinkProgram(p)
}

func testProgramLinkError(p GlObj) error {
	// Check if program had a link error.
	var success int32
	gl.GetProgramiv(p, gl.LINK_STATUS, &success)

	if success == gl.FALSE {
		var length GlInt32
		gl.GetProgramiv(p, gl.INFO_LOG_LENGTH, &length)
		log := make([]byte, length)
		gl.GetProgramInfoLog(p, length, nil, &log[0])
		return fmt.Errorf("link error: %s", gl.GoStr(&log[0]))
	}
	return nil
}

func createShader(stype GlUint32) GlObj {
	return gl.CreateShader(stype)
}

func setShaderSource(s GlObj, src string) {
	code, free := gl.Strs(src)
	length := int32(len(src))
	gl.ShaderSource(s, 1, code, &length)
	free()
}

func compileShader(s GlObj) {
	gl.CompileShader(s)
}

func testShaderCompileError(s GlObj) error {
	var success int32
	gl.GetShaderiv(s, COMPILE_STATUS, &success)

	if success == gl.FALSE {
		var length GlInt32
		gl.GetShaderiv(s, gl.INFO_LOG_LENGTH, &length)
		log := make([]byte, length)
		gl.GetShaderInfoLog(s, length, nil, &log[0])
		return fmt.Errorf("compile error: %s", gl.GoStr(&log[0]))
	}

	return nil
}

func useProgram(p GlObj) {
	gl.UseProgram(p)
}

func deleteProgram(p GlObj) {
	gl.DeleteProgram(p)
}

func genTexture() GlObj {
	var tex GlObj
	gl.GenTextures(1, &tex)
	return tex
}

func deleteTexture(t GlObj) {
	gl.DeleteTextures(1, &t)
}

func bindTexture(tt GlUint32, t GlObj) {
	gl.BindTexture(tt, t)
}

func texParameteri(tt, pname GlUint32, pval GlInt32) {
	gl.TexParameteri(tt, pname, pval)
}

func texImage2D(width, height GlInt32, pix []uint8) {
	gl.TexImage2D(
		TEXTURE_2D,
		0,
		int32(RGBA),
		width,
		height,
		0,
		RGBA,
		UNSIGNED_BYTE,
		gl.Ptr(pix),
	)
}

func genBuffer() GlObj {
	var buf GlObj
	gl.GenBuffers(1, &buf)
	return buf
}

func deleteBuffer(b GlObj) {
	gl.DeleteBuffers(1, &b)
}

func bindBuffer(btype GlUint32, buf GlObj) {
	gl.BindBuffer(btype, buf)
}

func bufferData(btype GlUint32, va []float32, dtype GlUint32) {
	gl.BufferData(btype, int(unsafe.Sizeof(float32(0)))*len(va), gl.Ptr(va), dtype)
}

func genVertexArray() GlObj {
	var vao GlObj
	gl.GenVertexArrays(1, &vao)
	return vao
}

func deleteVertexArray(vs GlObj) {
	gl.DeleteVertexArrays(1, &vs)
}

func bindVertexArray(va GlObj) {
	gl.BindVertexArray(va)
}

func vertexAttribPointer(i GlUint32, size GlInt32, atype GlUint32, norm bool, stride GlInt32, offset GlInt) {
	gl.VertexAttribPointer(i, size, atype, norm, int32(unsafe.Sizeof(float32(0)))*stride, gl.PtrOffset(offset*int(unsafe.Sizeof(float32(0)))))
}

func enableVertexAttribArray(i GlUint32) {
	gl.EnableVertexAttribArray(i)
}

func getUniformLocation(p GlObj, name string) GlLoc {
	return gl.GetUniformLocation(p, gl.Str(name+"\x00"))
}

func uniform1i(loc GlLoc, val GlInt32) {
	gl.Uniform1i(loc, val)
}

func uniformMatrix3fv(loc GlLoc, mat mgl32.Mat3) {
	gl.UniformMatrix3fv(loc, 1, false, &mat[0])
}
