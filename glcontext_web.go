//+build js
//+build !linux !windows !darwin !freebsd
//+build !android !ios

package alibi

import (
	"fmt"

	"github.com/go-gl/mathgl/mgl32"
	"github.com/gopherjs/gopherwasm/js"
)

var (
	gl     js.Value
	vaoExt GlObj
)

func newWebGLContext(canvas js.Value) {
	// Init webgl context.
	attrs := js.Global().Get("Object").New()
	attrs.Set("alpha", true)
	attrs.Set("premultipliedAlpha", true)

	gl = canvas.Call("getContext", "webgl", attrs)
	if gl == js.Null() {
		gl = canvas.Call("getContext", "experimental-webgl", attrs)
		if gl == js.Null() {
			panic("could not create webgl context")
		}
	}

	vaoExt = gl.Call("getExtension", webGlExtVao)
	if vaoExt == js.Null() {
		panic("webgl extension OES_vertex_array_object could not be enabled")
	}

	context = &glContext{}

	fmt.Println(context.Version())

	context.SetClearColor(0.8, 0.3, 0.01, 1)
	context.Clear(COLOR_BUFFER_BIT)
}

func setClearColor(r, g, b, a float32) {
	gl.Call("clearColor", r, g, b, a)
}

func clear(flags GlUint32) {
	gl.Call("clear", flags)
}

func version() string {
	return gl.Call("getParameter", VERSION).String()
}

func setViewport(x, y, width, height GlInt32) {
	gl.Call("viewport", x, y, width, height)
}

func deleteShader(s GlObj) {
	gl.Call("deleteShader", s)
}

func createProgram() GlObj {
	return gl.Call("createProgram")
}

func attachShader(prog, s GlObj) {
	gl.Call("attachShader", prog, s)
}

func detachShader(prog, s GlObj) {
	gl.Call("detachShader", prog, s)
}

func linkProgram(p GlObj) {
	gl.Call("linkProgram", p)
}

func testProgramLinkError(p GlObj) error {
	success := gl.Call("getProgramParameter", p, LINK_STATUS).Bool()
	if !success {
		log := gl.Call("getProgramInfoLog", p).String()
		return fmt.Errorf("link error: %s", log)
	}
	return nil
}

func createShader(stype GlUint32) GlObj {
	return gl.Call("createShader", stype)
}

func setShaderSource(s GlObj, src string) {
	gl.Call("shaderSource", s, src)
}

func compileShader(s GlObj) {
	gl.Call("compileShader", s)
}

func testShaderCompileError(s GlObj) error {
	success := gl.Call("getShaderParameter", s, COMPILE_STATUS).Bool()

	if !success {
		log := gl.Call("getShaderInfoLog", s)
		return fmt.Errorf("compile error: %s", log)
	}

	return nil
}

func useProgram(p GlObj) {
	gl.Call("useProgram", p)
}

func deleteProgram(p GlObj) {
	gl.Call("deleteProgram", p)
}

func genTexture() GlObj {
	return gl.Call("createTexture")
}

func deleteTexture(t GlObj) {
	gl.Call("deleteTexture", t)
}

func bindTexture(tt GlUint32, t GlObj) {
	gl.Call("bindTexture", tt, t)
}

func texParameteri(tt, pname GlUint32, pval GlInt32) {
	gl.Call("texParameteri", tt, pname, pval)
}

func texImage2D(width, height GlInt32, pix []uint8) {
	gl.Call(
		"texImage2D",
		TEXTURE_2D,
		0,
		RGBA,
		RGBA,
		UNSIGNED_BYTE,
		js.TypedArrayOf(pix),
	)
}

func genBuffer() GlObj {
	return gl.Call("createBuffer")
}

func deleteBuffer(b GlObj) {
	gl.Call("deleteBuffer", b)
}

func bindBuffer(btype GlUint32, buf GlObj) {
	gl.Call("bindBuffer", btype, buf)
}

func bufferData(btype GlUint32, va []float32, dtype GlUint32) {
	gl.Call("bufferData", btype, va, dtype)
}

func genVertexArray() GlObj {
	return vaoExt.Call("createVertexArrayOES")
}

func deleteVertexArray(va GlObj) {
	vaoExt.Call("deleteVertexArrayOES", va)
}

func bindVertexArray(va GlObj) {
	vaoExt.Call("bindVertexArray", va)
}

func vertexAttribPointer(i GlUint32, size GlInt32, atype GlUint32, norm bool, stride GlInt32, offset GlInt) {
	gl.Call("vertexAttribPointer", i, size, atype, norm, 0, offset*4)
}

func enableVertexAttribArray(i GlUint32) {
	gl.Call("enableVertexAttribArray", i)
}

func getUniformLocation(p GlObj, name string) GlLoc {
	return gl.Call("getUniformLocation", p, name)
}

func uniform1i(loc GlLoc, val GlInt32) {
	gl.Call("uniform1i", loc, val)
}

func uniformMatrix3fv(loc GlLoc, mat mgl32.Mat3) {
	gl.Call("uniformMatrix3fv", loc, false, mat)
}
