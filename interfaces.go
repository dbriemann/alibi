package alibi

// Window abstracts the container for the render target by defining a generic interface.
// The container may be a window on an operating system or a canvas in a browser (etc.).
type Window interface {
	Init(c *Config)
	Destroy()
	Closed() bool
	Update()
}
