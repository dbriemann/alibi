package alibi

type VertexArray struct {
	verts []float32
}

type AttributeFormat []Attribute

func (af AttributeFormat) Size() int {
	sum := 0
	for _, a := range af {
		sum += a.Type.Size()
	}
	return sum
}

type Attribute struct {
	Name string
	Type AttributeType
}

type AttributeType int

const (
	Int AttributeType = iota
	Float
	Vec2
	Vec3
	Vec4
)

// Size returns the size of the type in bytes.
func (at AttributeType) Size() int {
	switch at {
	case Int:
		return 4
	case Float:
		return 4
	case Vec2:
		return 2 * 4
	case Vec3:
		return 3 * 4
	case Vec4:
		return 4 * 4
	default:
		panic("unknown attribute type")
	}
}
