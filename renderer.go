package alibi

type Renderer struct {
	Program      *Program
	VertexFormat *AttributeFormat
}

func (r *Renderer) Begin() {
	context.UseProgram(r.Program.id)
}

func (r *Renderer) End() {
	context.UseProgram(NULL)
}
