package alibi

import (
	"fmt"

	"github.com/go-gl/mathgl/mgl32"
)

// Shader represents an glsl shader by id, source and type.
type Shader struct {
	Code  string
	SType GlUint32

	id GlObj
}

// NewShader creates a new shader object that holds all relevant
// information about the shader. It does not compile the shader.
func NewShader(code string, stype GlUint32) *Shader {
	s := &Shader{
		Code:  code,
		SType: stype,
		id:    NULL,
	}
	return s
}

// Compile compiles the shader and returns an error on failure.
func (s *Shader) Compile() error {
	if s.id != NULL {
		return fmt.Errorf("shader is already compiled")
	}
	s.id = context.CreateShader(s.SType)
	context.SetShaderSource(s.id, s.Code)

	return context.CompileShader(s.id)
}

// Delete deletes a compiled shader from gpu memory.
func (s *Shader) Delete() {
	context.DeleteShader(s.id)
	s.id = NULL
}

// Program represents a gl program by id and its linked shaders.
type Program struct {
	VertexShader   *Shader
	FragmentShader *Shader

	id GlObj
}

// NewProgram creates a new shader program from given vertex and fragment shaders.
// If a shader is nil it is replaced by the corresponding default shader.
// The compile status of the shaders will be identical to how it was before this
// function was invoked.
func NewProgram(vs, fs *Shader) (*Program, error) {
	p := &Program{}

	if vs == nil {
		vs = defaultVertexShader
	}
	if fs == nil {
		fs = defaultFragmentShader
	}

	p.id = context.CreateProgram()

	if vs.id == NULL {
		if err := vs.Compile(); err != nil {
			return nil, err
		}
		defer vs.Delete()

	}
	p.Attach(vs)

	if fs.id == NULL {
		if err := fs.Compile(); err != nil {
			return nil, err
		}
		defer fs.Delete()
	}
	p.Attach(fs)

	err := p.Link()

	return p, err
}

func (p *Program) Delete() {
	context.DeleteProgram(p.id)
}

func (p *Program) Begin() {
	context.UseProgram(p.id)
}

func (p *Program) End() {
	context.UseProgram(NULL)
}

func (p *Program) Attach(s *Shader) {
	context.AttachShader(p.id, s.id)
}

func (p *Program) Link() error {
	return context.LinkProgram(p.id)
}

func (p *Program) SetInt(name string, val GlInt32) {
	loc := context.GetUniformLocation(p.id, name)
	context.Uniform1i(loc, val)
}

func (p *Program) SetMatrix3(name string, mat mgl32.Mat3) {
	loc := context.GetUniformLocation(p.id, name)
	context.UniformMatrix3fv(loc, mat)
}

// TODO uniforms ..
