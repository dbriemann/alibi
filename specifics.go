package alibi

const (
	openGLMajor = 3
	openGLMinor = 3
)

var (
	ARRAY_BUFFER         GlUint32
	CLAMP_TO_EDGE        GlInt32
	COLOR_BUFFER_BIT     GlUint32
	COMPILE_STATUS       GlUint32
	DYNAMIC_DRAW         GlUint32
	ELEMENT_ARRAY_BUFFER GlUint32
	FRAGMENT_SHADER      GlUint32
	LINEAR               GlInt32
	LINK_STATUS          GlUint32
	MIRRORED_REPEAT      GlInt32
	NEAREST              GlInt32
	REPEAT               GlInt32
	RGB                  GlInt32
	RGBA                 GlUint32
	STATIC_DRAW          GlUint32
	TEXTURE_2D           GlUint32
	TEXTURE_MIN_FILTER   GlUint32
	TEXTURE_MAG_FILTER   GlUint32
	TEXTURE_WRAP_S       GlUint32
	TEXTURE_WRAP_T       GlUint32
	UNSIGNED_BYTE        GlUint32
	VERSION              GlUint32
	VERTEX_SHADER        GlUint32
	NULL                 GlObj
)

type Config struct {
	Title         string
	Width, Height int
	Fullscreen    bool
	VSync         bool
}
