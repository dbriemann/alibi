//+build js
//+build !linux !windows !darwin !freebsd
//+build !android !ios

package alibi

import "github.com/gopherjs/gopherwasm/js"

type (
	// GlBool   = js.Value
	GlUint32 = int
	GlInt32  = int
	GlInt    = int
	GlObj    = js.Value
	GlLoc    = js.Value
)

const (
	webGlExtVao = "OES_vertex_array_object"
)

func init() {
	c := js.Global().Get("WebGLRenderingContext").Get("prototype")
	ARRAY_BUFFER = GlUint32(c.Get("ARRAY_BUFFER").Int())
	CLAMP_TO_EDGE = GlUint32(c.Get("CLAMP_TO_EDGE").Int())
	COLOR_BUFFER_BIT = GlUint32(c.Get("COLOR_BUFFER_BIT").Int())
	COMPILE_STATUS = GlUint32(c.Get("COMPILE_STATUS").Int())
	DYNAMIC_DRAW = GlUint32(c.Get("DYNAMIC_DRAW").Int())
	ELEMENT_ARRAY_BUFFER = GlUint32(c.Get("ELEMENT_ARRAY_BUFFER").Int())
	FRAGMENT_SHADER = GlUint32(c.Get("FRAGMENT_SHADER").Int())
	LINEAR = GlUint32(c.Get("LINEAR").Int())
	LINK_STATUS = GlUint32(c.Get("LINK_STATUS").Int())
	MIRRORED_REPEAT = GlUint32(c.Get("MIRRORED_REPEAT").Int())
	NEAREST = GlUint32(c.Get("NEAREST").Int())
	REPEAT = GlUint32(c.Get("REPEAT").Int())
	RGB = GlUint32(c.Get("RGB").Int())
	RGBA = GlUint32(c.Get("RGBA").Int())
	TEXTURE_2D = GlUint32(c.Get("TEXTURE_2D").Int())
	TEXTURE_MIN_FILTER = GlUint32(c.Get("TEXTURE_MIN_FILTER").Int())
	TEXTURE_MAG_FILTER = GlUint32(c.Get("TEXTURE_MIN_FILTER").Int())
	TEXTURE_WRAP_S = GlUint32(c.Get("TEXTURE_WRAP_S").Int())
	TEXTURE_WRAP_T = GlUint32(c.Get("TEXTURE_WRAP_T").Int())
	UNSIGNED_BYTE = GlUint32(c.Get("UNSIGNED_BYTE").Int())
	VERSION = GlUint32(c.Get("VERSION").Int())
	VERTEX_SHADER = GlUint32(c.Get("VERTEX_SHADER").Int())
	NULL = js.Null()
}
