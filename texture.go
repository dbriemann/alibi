package alibi

import (
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"os"
)

type Texture struct {
	Width, Height GlInt

	id    GlObj
	wrapS GlInt32
	wrapT GlInt32
	min   GlInt32
	mag   GlInt32
}

func NewTexture() *Texture {
	t := &Texture{
		id: context.CreateTexture(),
	}

	t.Begin()
	t.SetScalingMethod(LINEAR, LINEAR)
	t.SetWrapType(REPEAT, REPEAT)
	t.End()

	return t
}

func (tex *Texture) Load(imgPath string) error {
	imgFile, err := os.Open(imgPath)
	if err != nil {
		return fmt.Errorf("image %s not found: %s", imgPath, err.Error())
	}

	img, _, err := image.Decode(imgFile)
	if err != nil {
		return err
	}

	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return fmt.Errorf("unsupported stride")
	}

	// slow...
	// TODO .. faster
	for x := 0; x < img.Bounds().Dx(); x++ {
		for y := 0; y < img.Bounds().Dy(); y++ {
			ny := (img.Bounds().Dy() - 1) - y
			rgba.Set(x, ny, img.At(x, y))
		}
	}

	tex.Begin()
	context.TexImage2D(GlInt32(rgba.Rect.Size().X), GlInt32(rgba.Rect.Size().Y), rgba.Pix)
	tex.End()

	return nil
}

func (tex *Texture) Delete() {
	context.DeleteTexture(tex.id)
}

func (tex *Texture) Begin() {
	// TODO save last tex for restoring ?
	context.BindTexture(TEXTURE_2D, tex.id)
}

func (tex *Texture) End() {
	// TODO restore last tex ?
	context.BindTexture(TEXTURE_2D, NULL)
}

func (tex *Texture) SetWrapType(s, t GlInt32) {
	tex.wrapS, tex.wrapT = s, t
	tex.Begin()
	context.SetTextureParamInt(TEXTURE_2D, TEXTURE_WRAP_S, s)
	context.SetTextureParamInt(TEXTURE_2D, TEXTURE_WRAP_T, t)
	tex.End()
}

func (tex *Texture) SetScalingMethod(min, mag GlInt32) {
	tex.min, tex.mag = min, mag
	tex.Begin()
	context.SetTextureParamInt(TEXTURE_2D, TEXTURE_MIN_FILTER, min)
	context.SetTextureParamInt(TEXTURE_2D, TEXTURE_MAG_FILTER, mag)
	tex.End()
}
