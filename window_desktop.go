//+build linux windows darwin freebsd
//+build !android !ios
//+build !js

package alibi

import (
	"github.com/go-gl/glfw/v3.2/glfw"
)

type desktopWindow struct {
	win    *glfw.Window
	config *Config
}

func NewWindow() *desktopWindow {
	return &desktopWindow{}
}

func (w *desktopWindow) Init(c *Config) {
	if err := glfw.Init(); err != nil {
		panic(err)
	}
	w.config = c

	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.ContextVersionMajor, openGLMajor)
	glfw.WindowHint(glfw.ContextVersionMinor, openGLMinor)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	window, err := glfw.CreateWindow(w.config.Width, w.config.Height, w.config.Title, nil, nil)
	if err != nil {
		panic(err)
	}

	w.win = window
	w.win.MakeContextCurrent()

	if w.config.VSync {
		glfw.SwapInterval(1)
	} else {
		glfw.SwapInterval(0)
	}

	newOpenGLContext()
}

func (w *desktopWindow) resizeCallback(win *glfw.Window, width, height GlInt32) {
	context.SetViewport(0, 0, width, height)
}

func (w *desktopWindow) Destroy() {
	glfw.Terminate()
}

func (w *desktopWindow) Closed() bool {
	return w.win.ShouldClose()
}

func (w *desktopWindow) Update() {
	glfw.PollEvents()
	w.win.SwapBuffers()
}
