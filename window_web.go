//+build js
//+build !linux !windows !darwin !freebsd
//+build !android !ios

package alibi

import (
	"github.com/gopherjs/gopherwasm/js"
)

type webWindow struct {
	canvas GlObj
	config *Config
}

func NewWindow() *webWindow {
	return &webWindow{}
}

func (w *webWindow) Init(c *Config) {
	w.config = c

	// Init browser canvas.
	document := js.Global().Get("document")
	// TODO -- maybe we should define a div in a template and use that instead.
	w.canvas = document.Call("createElement", "canvas")
	w.canvas.Set("width", w.config.Width)
	w.canvas.Set("height", w.config.Height)
	w.canvas.Get("style").Set("outline", "none")
	w.canvas.Get("style").Set("width", w.config.Width)
	w.canvas.Get("style").Set("height", w.config.Height)
	document.Get("body").Call("appendChild", w.canvas)

	newWebGLContext(w.canvas)
}

func (w *webWindow) Destroy() {

}

func (w *webWindow) Closed() bool {
	return false
}

func (w *webWindow) Update() {

}

// TODO resize callback?
